define(function(require){
	var $ = require('jquery');

	var tree = function() {
		var timer;
		$('#tree_list li.tree_item').hover(function(e) {
			timer && clearTimeout(timer);
			$(this).siblings().removeClass('tree_selected').end().addClass('tree_selected');
		}, function() {
			var self = this;
			timer = setTimeout(function() {
				$(self).removeClass('tree_selected');
			}, 200);
		});
	};


	$(function() {		
		tree();	
	});
});