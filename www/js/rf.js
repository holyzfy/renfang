$(function() {
	var $navSearch = $('div.navbar-search').closest('li').addClass('rf_nav_search');
	$navSearch.next('li').addClass('rf_nav_support').insertAfter('#adminLinks');
	$('ul.nav > li > a i.user').closest('li').attr('id', 'rf_nav_user');

	var $topNavItems = $('ul.top-nav-bar > li > a');
	$topNavItems.filter(':contains(主页)').closest('li').addClass('rf_nav_home');
	$topNavItems.filter(':contains(主机)').closest('li').addClass('rf_nav_host');
	$topNavItems.filter(':contains(审核)').closest('li').addClass('rf_nav_audit');

});